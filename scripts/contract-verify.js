const hre = require("hardhat");
async function main(){
    await hre.run("verify:verify", {
        address: "0x39e546B0037Af7E92F9d221DC36D92FF33F92ac0",
        constructorArguments: []
    });
}
main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });