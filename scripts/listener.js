const ethers = require("ethers");
const contractSol = require("../src/artifacts/contracts/Counter.sol/Counter.json");
require("dotenv").config();
async function main() {
    const provider = new ethers.providers.WebSocketProvider(
        "wss://eth-goerli.g.alchemy.com/v2/RMla1rxGzj9uB_Ow7m7A_Klp_wyZygs8",
    )
    const contract = new ethers.Contract("0xdd0991d0753200448258f0D7Bc46d4782F45977a", contractSol.abi, provider)

    contract.on("Updated", (value) => {
        console.log("updated")
    })
    console.log("listening...")
}
main();