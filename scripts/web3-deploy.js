const {ethers, BigNumber} = require("ethers")

const API_URL="https://eth-goerli.g.alchemy.com/v2/RMla1rxGzj9uB_Ow7m7A_Klp_wyZygs8"
const API_KEY="RMla1rxGzj9uB_Ow7m7A_Klp_wyZygs8"
const PRIVATE_KEY="5e61f299fe5a72008a8f11f1b7b052d249f647102232f6610239f563177c4c28"
const PUBLIC_KEY="0xBc23a77171905a86c96D2aFBB92f10Fb172f9A61"
const CONTRACT_COUNTER_ADDRESS="0x87fa2A0f851490023D089bD0953008EFd4a1239e"

const solContract = require("../src/artifacts/contracts/Counter.sol/Counter.json");

async function main() {
    const provider = new ethers.providers.AlchemyProvider("goerli", API_KEY)
    const signer = new ethers.Wallet(`0x${PRIVATE_KEY}`, provider)

    const contractFactory = ethers.ContractFactory.fromSolidity(solContract, signer)
    // const contract = await contractFactory.deploy(42)
    // console.log(contract.address)
    const contract = contractFactory.attach(CONTRACT_COUNTER_ADDRESS)

    const increment = () => contract.increment()
    const getCounter = () => contract.getCounter()
    const estimateGas = (methodName) => contract.estimateGas[methodName]()
    const tx = await increment()
    tx.wait()
    // console.log((await estimateGas("increment")).toString())



}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });