// Author: Skiminok
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract Counter {
    uint public count;
    event Updated(uint oldValue, uint newValue);
    constructor(uint value){
     count = value;
    }
    function increment() public payable {
        emit Updated(count, count+1);
        count +=1;
    }

    function getCounter()
    public view
    returns(uint){
        return count;
    }
}
