import {useContext} from "react";
import {Web3Context} from "../App";
import {ethers} from "ethers";

const Profile = (props) => {
    return <div>
        <h3>Profile</h3>
        <div>address: {props.address}</div>
        <div>balance: {props.balance}</div>
        <br/>
        <br/>
    </div>
}
const AuthWalletButton = (props) => {
    return <button
        onClick={async () => {
            try {
                const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
                await window.ethereum.send("eth_requestAccounts");
                const signer = provider.getSigner();
                props.setSigner(signer)
            } catch (e) {
                console.log("denied")
            }
        }}
    >
        <span>connect the wallet</span>
    </button>
}

const Wallet = () => {
    const [provider ,signer, setSigner] = useContext(Web3Context)
    return <div>
        {   signer
            ? <Profile address={signer.address}/>
            : <AuthWalletButton setSigner={setSigner} provider={provider}/>
        }
    </div>
}
export default Wallet