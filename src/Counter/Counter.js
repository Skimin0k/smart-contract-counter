import {useContext, useEffect, useState} from "react";
import {Web3Context} from "../App";
import {ethers} from "ethers";
import {BigNumber, Signer} from "ethers";
import contractSol from "../artifacts/contracts/Counter.sol/Counter.json"
import {Provider} from "@ethersproject/abstract-provider";

class Auction {
  getLots() {
    return
  }

}

const Counter = () => {
    const [provider, signer] = useContext(Web3Context)
    const [contract, setContact] = useState(undefined)
    useEffect(() => {
        if (Provider.isProvider(provider) || Signer.isSigner(signer)) {
            console.log(`updated to ${Signer.isSigner(signer)? "signer" : "provider"}`)
            setContact(
                ethers.ContractFactory.fromSolidity(contractSol)
                    .attach(process.env.REACT_APP_CONTRACT_COUNTER_ADDRESS)
                    .connect(
                        Signer.isSigner(signer)? signer : provider
                    )
            )
        }
    }, [provider, signer])
    useEffect(() => {
        if(contract) {
            contract.getCounter().then(value => {
                setValue(value.toString())
            })
        }
    }, [contract])

    useEffect(() => {
        const alchemyWebSocketProvider = new ethers.providers.AlchemyWebSocketProvider("goerli", process.env.REACT_APP_API_KEY)
        const ListenerContract = ethers.ContractFactory.fromSolidity(contractSol)
            .attach(process.env.REACT_APP_CONTRACT_COUNTER_ADDRESS)
            .connect(alchemyWebSocketProvider)
        ListenerContract.on("Updated", (oldValue, newValue) => {
            setValue(newValue.toString())
        })
    }, [])

    const [value, setValue] = useState("nan")

    return <div>
        <h2>Counter</h2>
        <div>current value is: {value}</div>
        <button
            disabled={!Signer.isSigner(signer)}
            onClick={() => {
                contract.increment()
            }}
        ><span>increment value</span></button>
        <button
            disabled={!Signer.isSigner(signer)}
            onClick={async () => {
                contract.getCounter().then(value => {
                    setValue(value.toString())
                })
            }}
        ><span>update value</span></button>
    </div>
}
export default Counter