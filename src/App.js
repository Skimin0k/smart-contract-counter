import './App.css';
import {ethers} from "ethers";
import {createContext, useState} from "react";
import Counter from "./Counter/Counter";
import Wallet from "./Wallet/Wallet";

const useWeb3 = (network, apiKey) => {
    const [provider,] = useState(new ethers.providers.AlchemyWebSocketProvider(network, apiKey))
    const [signer, setSigner] = useState(undefined)
    return [provider, signer, setSigner]
}


export const Web3Context = createContext();

function App() {
    const [provider, signer, setSigner] = useWeb3("goerli", process.env.REACT_APP_API_KEY)
    return (
        <div className="App">
            <Web3Context.Provider value={[provider, signer, setSigner]}>
                <Wallet/>
                <Counter/>
            </Web3Context.Provider>
        </div>
    );
}

export default App;
