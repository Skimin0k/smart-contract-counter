const {expect, use} = require('chai');
const {deployContract, MockProvider, solidity} = require('ethereum-waffle');

const Counter = require('../../src/artifacts/contracts/Counter.sol/Counter.json')

use(solidity);

describe('BasicToken', () => {
    const [wallet] = new MockProvider().getWallets();
    let token;
    beforeEach(async () => {
        token = await deployContract(wallet, Counter, [1000]);
    });
    it('Assigns initial balance', async () => {
        expect(await token.getCounter()).to.equal(1000);
    });
    it('emit update', async () => {
        expect(await token.increment()).to.emit(token, "Updated");
        expect(await token.getCounter()).to.equal(1001);
    });
})